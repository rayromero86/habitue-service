@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Adding New Product</div>

                <div class="card-body">
                    <admin-product-add></admin-product-add>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
