@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

            <admin-product-view :product="{{$product}}"></admin-product-view>
            
        </div>
    </div>
</div>
@endsection
