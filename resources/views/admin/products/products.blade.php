@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Products</div>

                <div class="card-body">
                    <a href="{{ route('admin.product.add') }}">Add new product</a>

                    <br /><br /><br />

                    <h3>Products:</h3>
                    <ul>
                        @foreach($products as $product)
                            <li><a href="{{ route('admin.product.show', $product->slug) }}">{{ $product->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
