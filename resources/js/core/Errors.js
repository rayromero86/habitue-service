class Errors {
    /**
     * Create a new Errors instance.
     */
    constructor() {
        this.errors = {};
    }


    /**
     * Determine if an errors exists for the given field.
     *
     * @param {string} field
     */
    has(field) {
        return this.errors.hasOwnProperty(field);
    }


    /**
     * Determine if an errors exists for the given field AND the value is not empty.
     *
     * @param {string} field
     */
    hasNotEmpty(field) {
        if (!this.errors.hasOwnProperty(field)) {
            return false;
        }

        if (!this.errors[field]) {
            return false;
        }

        return this.errors[field][0].length > 0;
    }

    /**
     * Search for a substring of key and determine if an errors exists.
     *
     * @param {string} substring
     */
    hasAWord(substring) {
        var x;
        for (x in this.errors) {
            if (x.search(substring) >= 0 && this.errors[x][0]) {
                return true;
            }
        }

        return false;
    }


    /**
     * Determine if we have any errors.
     */
    any() {
        return Object.keys(this.errors).length > 0;
    }


    /**
     * Retrieve the error message for a field.
     *
     * @param {string} field
     */
    get(field) {
        if (this.errors[field]) {
            return this.errors[field][0];
        }
    }


    /**
     * Search for a substring of key and retrieve the error message for that field.
     *
     * @param {string} field
     */
    searchAndGet(substring) {
        var x;
        for (x in this.errors) {
            if (x.search(substring) >= 0 && this.errors[x][0]) {
                return this.errors[x][0];
            }
        }
    }


    /**
     * Record the new errors.
     *
     * @param {object} errors
     */
    record(errors) {
        this.errors = errors;
    }


    /**
     * Clear one or all error fields.
     *
     * @param {string|null} field
     */
    clear(field) {
        if (field) {
            delete this.errors[field];

            return;
        }

        this.errors = {};
    }
}


export default Errors;