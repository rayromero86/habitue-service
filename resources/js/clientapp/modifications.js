import Vue from 'vue'

Vue.filter('currency_pound', function (value) {
    if (!value) return value

    return '£' + parseFloat(value).toFixed(2)
})