import Vue from 'vue'
import Vuex from 'vuex'

// import createLogger /from '../../../src/plugins/logger'

// Modules
import general from './modules/general'
import auth from './modules/auth'
import signup from './modules/signup'
import profile from './modules/profile'
import user from './modules/user'
import products from './modules/products'
import tickets from './modules/tickets'
import billingcards from './modules/billingcards'
import purchase from './modules/purchase'

Vue.use(Vuex)

// const debug = process.env.MIX_VUE_ENV !== 'production'
const debug = false

export default new Vuex.Store({
    modules: {
        general,
        auth,
        signup,
        profile,
        user,
        products,
        tickets,
        billingcards,
        purchase,
    },
    strict: debug,
    // plugins: debug ? [createLogger()] : []

    state: {
        env: process.env.MIX_VUE_ENV, // `development` | `production`
    },
})