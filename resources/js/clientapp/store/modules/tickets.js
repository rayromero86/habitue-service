import tickets from './../../api/tickets'

// initial state
const state = {
    status: 'initial',
    create_status: 'initial',
    all: [],
    error: {
        status: null,
        statusText: null,
        data: {
            message: null
        },
    },
}
  
const getters = {
    status_is_ready: state => state.status != 'busy',

    create_status_is_ready: state => state.create_status != 'busy',

    all: state => state.all,

    is_loaded: state => state.all.length > 0,
}

const mutations = {
    populateTickets(state, payload) {
        state.status = 'success'

        state.all = payload.data
    },

    onCreateSuccess(state, payload) {
        state.create_status = 'success'
    },

    onError(state, payload) {
        state.status = 'failed'

        state.error = {
            status: payload.status,
            statusText: payload.statusText,
            data: payload.data,
        }
    },

    clearError(state) {
        state.error = {
            status: null,
            statusText: null,
            data: {
                message: null
            },
        }
    },
}

const actions = {
    init(context) {
        if (context.state.all.length < 1 && context.getters.status_is_ready === true) {
            context.dispatch('getAll')
        }
    },

    getAll(context) {
        // prepare
        context.commit('clearError')
        context.state.status = 'busy'

        tickets.getAll()
        .then((response) => {
            // console.log('response:',response)
            context.commit('populateTickets', response)
        })
        .catch((error) => {
            // console.log('error:',error)
            context.commit('onError', error)
        })
    },

    refreshAll(context) {
        context.dispatch('getAll')
    },

    create(context, form_data) {
        // prepare
        context.commit('clearError')
        context.state.create_status = 'busy'

        tickets.create(form_data)
        .then((response) => {
            // console.log('response:',response)
            context.commit('onCreateSuccess', response)
            context.dispatch('getAll')
        })
        .catch((error) => {
            // console.log('error:',error)
            context.commit('onError', error)
        })
    },
}
  
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}