// initial state
const state = {
    app_name: 'Habitue',
    
    side_bar_items: [
        { 
            name: 'Dashboard', 
            link: '/app/dashboard/dash',
            icon: 'columns' 
        },
        { 
            name: 'Add-ons', 
            link: '/app/dashboard/categories',
            icon: 'puzzle-piece' 
        },
        { 
            name: 'Packages', 
            link: '/app/dashboard/packages',
            icon: 'box' 
        },
        { 
            name: 'Subscriptions', 
            link: '/app/dashboard/subscriptions/',
            icon: 'redo' 
        },
        { 
            name: 'Chat', 
            link: '/app/dashboard/chat/',
            icon: 'comments' 
        }, 
        { 
            name: 'Tickets', 
            link: '/app/dashboard/tickets/',
            icon: 'ticket-alt' 
        },
        { 
            name: 'Drive', 
            link: '/app/dashboard/drive/',
            icon: 'folder' 
        },
        { 
            name: 'Learn', 
            link: '/app/dashboard/learn/',
            icon: 'graduation-cap' 
        },
        { 
            name: 'Settings', 
            link: '/app/dashboard/settings/',
            icon: 'cog' 
        },
    ],
    
    profile_dropdown_visibility: false,

    profile_dropdown_items: [
        { 
            name: 'Profile', 
            link: '/app/dashboard/settings/profile',
        },
        { 
            name: 'Cards', 
            link: '/app/dashboard/settings/cards',
        },
        {
            name: 'Logout',
            link: '#'
        }
    ],

    notification_bar_visibility: false,

    notification_bar_items: [
        { 
            name: 'New Purchase', 
            message: 'Your new purchase is read. Please click and fill out the form', 
            link: '#',
            icon: 'columns' 
        },
        { 
            name: 'Ticket Reply', 
            message: 'Your support ticket has been replied to. Please click for futher details', 
            link: '#',
            icon: 'columns' 
        },
    ],

    messages_bar_visibility: false,

    messages_bar_items: [
        { 
            name: 'Jacky Smith', 
            message: 'Hi, we\'re having an issues with our Facebook ads not showing on mobile', 
            link: '#',
            icon: 'columns' 
        },
        { 
            name: 'Amy Wilson', 
            message: 'Is it possible for me to change the copy on the advert we put out on Tuesday?', 
            link: '#',
            icon: 'columns' 
        },
    ],

    settings_page_items: [
        { 
            name: 'Profile', 
            sub: 'Edit your profile details', 
            link: '/app/dashboard/settings/profile',
            icon: 'edit', 
        },
        { 
            name: 'Cards', 
            sub: 'Edit your profile details', 
            link: '/app/dashboard/settings/cards',
            icon: 'credit-card', 
        },
        { 
            name: 'Invoices', 
            sub: 'Edit your profile details', 
            link: '/app/dashboard/settings/invoices',
            icon: 'credit-card', 
        },
        { 
            name: 'Connect', 
            sub: 'Connect your Ad Accounts', 
            link: '/app/dashboard/settings/connect/',
            icon: 'plug', 
        },
    ],

    //
}

const getters = {
    profile_dropdown_visibility: state => state.profile_dropdown_visibility,

    notification_bar_visibility: state => state.notification_bar_visibility,

    messages_bar_visibility: state => state.messages_bar_visibility,
}

const mutations = {
    toggleProfileDropdownVisibility(state) {
        state.profile_dropdown_visibility = ! state.profile_dropdown_visibility
    },
  
    toggleNotificationBarVisibility(state) {
        state.notification_bar_visibility= ! state.notification_bar_visibility
    },

    toggleMessageBarVisibility(state) {
        state.messages_bar_visibility = ! state.messages_bar_visibility
    },
}
  
const actions = {
    xxx() {
        //
    }
}
  
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}