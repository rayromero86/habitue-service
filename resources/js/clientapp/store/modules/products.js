import products from './../../api/products'

// initial state
const state = {
    status: 'initial',
    all: [],
    error: {
        status: null,
        statusText: null,
        data: {
            message: null
        },
    },
}
  
const getters = {
    status_is_ready: state => state.status != 'busy',

    all: state => state.all,

    // get: (state, slug) => {
    //     'xx:'.slug
    // },

    is_loaded: state => state.all.length > 0,
}

const mutations = {
    populateProducts(state, payload) {
        state.status = 'success'

        state.all = payload.data
    },

    onError(state, payload) {
        state.status = 'failed'

        state.error = {
            status: payload.status,
            statusText: payload.statusText,
            data: payload.data,
        }
    },

    clearError(state) {
        state.error = {
            status: null,
            statusText: null,
            data: {
                message: null
            },
        }
    },
}

const actions = {
    init(context) {
        if (context.state.all.length < 1 && context.getters.status_is_ready === true) {
            context.dispatch('getAll')
        }
    },

    getAll(context) {
        // prepare
        context.commit('clearError')
        context.state.status = 'busy'

        products.getAll()
        .then((response) => {
            // console.log('response:',response)
            context.commit('populateProducts', response)
        })
        .catch((error) => {
            // console.log('error:',error)
            context.commit('onError', error)
        })
    },

    refreshAll(context) {
        context.dispatch('getAll')
    }
}
  
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}