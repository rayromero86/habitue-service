import profile from './../../api/profile'

// initial state
const state = {
    status: 'initial',
    status_text: '',
    details: {},
    error: {
        status: null,
        statusText: null,
        data: {
            message: null
        },
    },
}

const getters = {
    status_is_ready: state => state.status != 'busy',

    status_text: state => state.status_text,

    details: state => state.details,

    get: (state, key) => state.details.key,

    is_loaded: state => JSON.stringify(state.details) !== JSON.stringify({}),
}

const mutations = {
    populateProfile(state, payload) {
        state.status = 'success'

        state.details = Object.assign({id:payload.data.id}, payload.data.attributes)
    },

    onProfileUpdateSuccess(state, payload) {
        //
    },

    onError(state, payload) {
        state.status = 'failed'
        state.status_text = 'error encountered'

        state.error = {
            status: payload.status,
            statusText: payload.statusText,
            data: payload.data,
        }
    },

    clearError(state) {
        state.error = {
            status: null,
            statusText: null,
            data: {
                message: null
            },
        }
    },
}
  
const actions = {
    init(context, user_id) {
        if (context.state.is_loaded != true && context.getters.status_is_ready === true) {
            context.dispatch('get', user_id)
        }
    },

    get(context, user_id) {
        // prepare
        context.commit('clearError')
        context.state.status = 'busy'
        context.state.status_text = 'Loading profile'

        profile.get(user_id)
        .then((response) => {
            // console.log('response:',response)
            context.state.status_text = 'Profile loaded'
            context.commit('populateProfile', response)
        })
        .catch((error) => {
            // console.log('error:',error)
            context.commit('onError', error)
        })
    },

    getFresh(context, user_id) {
        // prepare
        context.commit('clearError')
        context.state.status = 'busy'
        context.state.status_text = 'Loading profile'

        profile.get(user_id)
        .then((response) => {
            // console.log('response:',response)
            context.state.status_text = 'Profile loaded'
            context.commit('populateProfile', response)
        })
        .catch((error) => {
            // console.log('error:',error)
            context.commit('onError', error)
        })
    },

    update(context, form_data) {
        // prepare
        context.commit('clearError')
        context.state.status = 'busy'
        context.state.status_text = 'Updating profile'

        form_data.user_id = context.state.details.id

        profile.update(form_data)
        .then((response) => {
            // console.log('response:',response)
            context.state.status_text = 'Profile updated'
            context.state.status = 'success'
        })
        .catch((error) => {
            // console.log('error:',error)
            context.state.status = 'failed'
        })
    },
}
  
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}