import purchase from './../../api/purchase'

// initial state
const state = {
    status: 'initial',
    all: [],
    error: {
        status: null,
        statusText: null,
        data: {
            status: null,
            message: null,
            error: {}
        },
    },
}
  
const getters = {
    status_is_ready: state => state.status != 'busy',
}

const mutations = {
    onCheckoutSuccess(state, payload) {
        state.status = 'success'

        // state.all = payload.data
    },

    onError(state, payload) {
        state.status = 'failed'

        state.error = {
            status: payload.status,
            statusText: payload.statusText,
            data: payload.data,
        }
    },

    clearError(state) {
        state.error = {
            status: null,
            statusText: null,
            data: {
                status: null,
                message: null,
                error: {}
            },
        }
    },
}

const actions = {
    checkout(context, form_data) {
        // prepare
        context.commit('clearError')
        context.state.status = 'busy'

        purchase.checkout(form_data)
        .then((response) => {
            console.log('response:',response)
            // context.commit('populateProducts', response)
        })
        .catch((error) => {
            console.log('error:',error)
            context.commit('onError', error)
        })
    },
}
  
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}