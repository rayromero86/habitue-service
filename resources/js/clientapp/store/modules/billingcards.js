import billingcards from './../../api/billingcards'

// initial state
const state = {
    status: 'initial',
    create_status: 'initial',
    all: [],
    error: {
        status: null,
        statusText: null,
        data: {
            status: null,
            message: null,
            error: {}
        },
    },
}
  
const getters = {
    status_is_ready: state => state.status != 'busy',

    create_status_is_ready: state => state.create_status != 'busy',

    all: state => state.all,

    is_loaded: state => state.all.length > 0,
}

const mutations = {
    // populateCards(state, payload) {
    //     state.status = 'success'

    //     state.all = payload.data
    // },

    // onCreateSuccess(state, payload) {
    //     state.create_status = 'success'
    // },

    onError(state, payload) {
        state.status = 'failed'

        state.error = {
            status: payload.status,
            statusText: payload.statusText,
            data: payload.data,
        }
    },

    clearError(state) {
        state.error = {
            status: null,
            statusText: null,
            data: {
                status: null,
                message: null,
                error: {}
            },
        }
    },
}

const actions = {
    init(context) {
        if (context.state.all.length < 1 && context.getters.status_is_ready === true) {
            context.dispatch('getAll')
        }
    },

    getAll(context, form_data) {
        // prepare
        context.commit('clearError')
        context.state.status = 'busy'

        billingcards.getAll(form_data)
        .then((response) => {
            console.log('response:',response)
            // context.commit('populateTickets', response)
        })
        .catch((error) => {
            console.log('error:',error)
            context.commit('onError', error)
        })
    },

    add(context, form_data) {
        // prepare
        context.commit('clearError')
        context.state.create_status = 'busy'

        billingcards.add(form_data)
        .then((response) => {
            console.log('response:',response)
            // context.commit('onCreateSuccess', response)
            context.dispatch('getAll')
        })
        .catch((error) => {
            console.log('error:',error)
            context.commit('onError', error)
        })
    },
}
  
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}