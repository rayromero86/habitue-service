
// initial state
const state = {
    xxx: 'xxx',
}

const getters = {
    xxx: state => state.xxx
}

const mutations = {
    xx1(state, val) {
        //
    },
  
    xx2(state, val) {
        //
    },
}
  
const actions = {
    xxx(context, val) {
        //
    }
}
  
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}