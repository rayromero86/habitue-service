import auth from './../../api/auth'

// initial state
const state = {
    status: 'initial',
    login_key: null,
    user: {
        id: null,
        slug: null,
        email: null,
        first_name: null,
        last_name: null,
    },
    error: {
        status: null,
        statusText: null,
        data: {
            status: null,
            message: null,
            error: {}
        },
    },
}

const getters = {
    status: state => state.status,

    isAuth: state => state.login_key != null,

    login_key: state => state.login_key,

    user_id: state => state.user.id,

    full_name: state => `${state.user.first_name} ${state.user.last_name}`,

    errors: state => state.error.data.error,
}

const mutations = {
    login(state, payload) {
        state.status = 'authenticated'

        state.login_key = payload.login_key

        state.user = {
            id: payload.data.id,
            slug: payload.data.slug,
            email: payload.data.attributes.email,
            first_name: payload.data.attributes.first_name,
            last_name: payload.data.attributes.last_name
        }

        axios.defaults.headers.common["Authorization"] = `Bearer ${state.login_key}`
    },
  
    logout(state) {
        state.status = 'logout'
        state.login_key = null
        state.user = {
            id: null,
            slug: null,
            email: null,
            first_name: null,
            last_name: null,
        }
    },

    onError(state, payload) {
        state.status = 'failed'

        state.error = {
            status: payload.status,
            statusText: payload.statusText,
            data: payload.data,
        }
    },

    clearError(state) {
        state.error = {
            status: null,
            statusText: null,
            data: {
                status: null,
                message: null,
                error: {}
            },
        }
    },
}
  
const actions = {
    login(context, data) {
        context.commit('clearError')
        context.state.status = 'logging'

        auth.tryLoggingIn(data)
        .then((response) => {
            // console.log('response:',response)
            context.commit('login', response)
        })
        .catch((error) => {
            // console.log('error:',error)
            context.commit('onError', error)
        })
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}