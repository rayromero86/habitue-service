import auth from './../../api/auth'
import states from './../../repositories/states'

// initial state
const state = {
    status: 'initial',
    error: {
        status: null,
        statusText: null,
        data: {
            status: null,
            message: null,
            error: {}
        },
    },
    states: states.data,
}

const getters = {
    status: state => state.status,

    errors: state => state.error.data.errors,

    states: state => state.states,
}

const mutations = {
    registered(state, payload) {
        state.status = 'success'
    },

    onError(state, payload) {
        state.status = 'failed'

        state.error = {
            status: payload.status,
            statusText: payload.statusText,
            data: payload.data,
        }
    },

    clearError(state) {
        state.error = {
            status: null,
            statusText: null,
            data: {
                status: null,
                message: null,
                error: {}
            },
        }
    },
}
  
const actions = {
    signup(context, form_data) {
        context.commit('clearError')
        context.state.status = 'busy'

        auth.signup(form_data)
        .then((response) => {
            // console.log('response:',response)
            context.commit('registered', response)
        })
        .catch((error) => {
            // console.log('error:',error)
            context.commit('onError', error)
        })
    }
}
  
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}