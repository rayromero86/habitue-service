// import profile from './../../api/profile'

// initial state
const state = {
    xxx: 'xxx',
}

const getters = {
    xxx: state => state.xxx
}

const mutations = {
    xx1() {
        //
    },
  
    xx2() {
        //
    },
}
  
const actions = {
    xxx() {
        //
    }
}
  
export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}