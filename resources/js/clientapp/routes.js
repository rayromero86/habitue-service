import Home from './views/Home'
import Login from './views/auth/Login'
import Register from './views/auth/Register'
import Dashboard from './views/dashboard/Dashboard'

import About from './views/dashboard/About'
import Dash from './views/dashboard/Dash'

import Categories from './views/dashboard/addons/Categories'
import Addon from './views/dashboard/addons/Addon'
import CategoryDetails from './views/dashboard/addons/CategoryDetails'
import Checkout from './views/dashboard/addons/Checkout.vue'

import Settings from './views/dashboard/settings/Settings'

import Cards from './views/dashboard/settings/cards/Cards'
import NewCard from './views/dashboard/settings/cards/New'
// import Card from './views/dashboard/settings/cards/Card'

import Invoices from './views/dashboard/settings/Invoices'
import Invoice from './views/dashboard/settings/Invoice'
import Connect from './views/dashboard/settings/Connect'
import Profile from './views/dashboard/settings/Profile'

import Packages from './views/dashboard/packages/Packages'

import Tickets from './views/dashboard/tickets/Tickets'

import Chat from './views/dashboard/chats/Chat'

import Subscriptions from './views/dashboard/subscriptions/Subscriptions'

import Drive from './views/dashboard/drive/Drive'

import Learn from './views/dashboard/learns/Learn'


export const routes = [
    {
        name: 'home',
        path: '/app',
        component: Home
    },
    {
        name: 'login',
        path: '/app/login',
        component: Login
    },
    {
        name: 'register',
        path: '/app/signup',
        component: Register
    },
    {
        name: 'dashboard',
        path: '/app/dashboard',
        component: Dashboard,
        meta: {
            requiresAuth: true
        },
        children: [
            {
                name: 'about',
                path: 'about',
                component: About
            },
            {
                name: 'dash',
                path: 'dash',
                component: Dash
            },
            // Addons
            {
                name: 'categories',
                path: 'categories',
                component: Categories
            },
            {
                name: 'addon',
                path: 'categories/:slug',
                component: Addon
            },
            {
                name: 'checkout',
                path: 'checkout',
                component: Checkout
            },
            {
                name: 'details',
                path: 'categories/details/:slug',
                component: CategoryDetails
            },
            // Cards
            {
                name: 'cards',
                path: 'settings/cards',
                component: Cards,
                children: [
                    {
                        name: 'card-new',
                        path: 'new',
                        component: NewCard
                    }
                ]
            },
            {
                name: 'settings',
                path: 'settings',
                component: Settings,
            },
            { 
                name: 'invoices',
                path: 'settings/invoices',
                component: Invoices
            },
            {
                name: 'invoice',
                path: 'settings/invoices/invoice/:id',
                component: Invoice
            },
            {
                name: 'profile',
                path: 'settings/profile',
                component: Profile
            },
            { 
                name: 'connect',
                path: 'settings/connect',
                component: Connect
            },
            {
                name: 'packages',
                path: 'packages',
                component: Packages
            },
            {
                name: 'tickets',
                path: 'tickets',
                component: Tickets
            },
            { 
                name: 'chat',
                path: 'chat',
                component: Chat
            },
            { 
                name: 'subscriptions',
                path: 'subscriptions',
                component: Subscriptions
            },
            {
                name: 'drive',
                path: 'drive',
                component: Drive
            },
            {
                name: 'learn',
                path: 'learn',
                component: Learn
            },
        ]
    },
];