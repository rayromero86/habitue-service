require('./bootstrap');

// setup vue and its dependecy add-ons
import Vue from 'vue'
import router from './router'
import store from './store/index'
import { initialize } from './init';
import ClientApp from './views/App'
require('./modifications')


// initialize
initialize(store, router)


const app = new Vue({
    el: '#clientapp',
    store,
    router,
    components: {ClientApp},
    // render: h => h(ClientApp),
})//.$mount('#clientapp');
