export default {
    getAll(_form_data) {
        let form_data = new FormData()
        form_data.append('method', 'get_billing_cards')
        form_data.append('user_id', _form_data.user_id)

        return new Promise((resolve, reject) => {
            axios.post(
                '/api/v1/service', form_data
            )
            .then(response => {
                // console.log('response:',response)
                if (response.data.status == 1 && response.data.message == 'success') {
                    return resolve(response.data)
                }
                else {
                    return reject(response.data)
                }
            })
            .catch(error => {
                // console.log('error:',error.response)
                return reject(error.response)
            })
        })
    },

    add(_form_data) {
        let form_data = new FormData()
        form_data.append('method', 'add_billing_card')
        form_data.append('user_id', _form_data.user_id)
        form_data.append('number', _form_data.number)
        form_data.append('exp_month', _form_data.exp_month)
        form_data.append('cvc', _form_data.cvc)
        form_data.append('exp_year', _form_data.exp_year)

        return new Promise((resolve, reject) => {
            axios.post(
                '/api/v1/service', form_data
            )
            .then(response => {
                // console.log('response:',response)
                if (response.data.status == 1 && response.data.message == 'success') {
                    return resolve(response.data)
                }
                else {
                    return reject(response.data)
                }
            })
            .catch(error => {
                // console.log('error:',error.response)
                return reject(error.response)
            })
        })
    }
}