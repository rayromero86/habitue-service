export default {
    checkout(_form_data) {
        let form_data = new FormData()
        form_data.append('method', 'checkout_product')
        form_data.append('product_slug', _form_data.product_slug)
        form_data.append('selected', _form_data.selected)
        form_data.append('total', _form_data.total)

        return new Promise((resolve, reject) => {
            axios.post(
                '/api/v1/service', form_data
            )
            .then(response => {
                // console.log('response:',response)
                if (response.data.status == 1 && response.data.message == 'success') {
                    return resolve(response.data)
                }
                else {
                    return reject(response.data)
                }
            })
            .catch(error => {
                // console.log('error:',error.response)
                return reject(error.response)
            })
        })
    }
}