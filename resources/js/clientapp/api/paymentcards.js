export default {
    getAll() {
        let form_data = new FormData()
        form_data.append('method', 'get_paymentcards')

        return new Promise((resolve, reject) => {
            axios.post(
                '/api/v1/service', form_data
            )
            .then(response => {
                // console.log('response:',response)
                if (response.data.status == 1 && response.data.message == 'success') {
                    return resolve(response.data)
                }
                else {
                    return reject(response.data)
                }
            })
            .catch(error => {
                // console.log('error:',error.response)
                return reject(error.response)
            })
        })
    },

    create(_form_data) {
        let form_data = new FormData()
        form_data.append('method', 'create_paymentcard')
        form_data.append('ref_id', _form_data.ref_id)
        form_data.append('title', _form_data.title)

        return new Promise((resolve, reject) => {
            axios.post(
                '/api/v1/service', form_data
            )
            .then(response => {
                // console.log('response:',response)
                if (response.data.status == 1 && response.data.message == 'success') {
                    return resolve(response.data)
                }
                else {
                    return reject(response.data)
                }
            })
            .catch(error => {
                // console.log('error:',error.response)
                return reject(error.response)
            })
        })
    }
}