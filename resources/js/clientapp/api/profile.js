export default {
    get(user_id) {
        let form_data = new FormData()
        form_data.append('method', 'get_user_by_id')
        form_data.append('user_id', user_id)

        return new Promise((resolve, reject) => {
            axios.post(
                '/api/v1/service', form_data
            )
            .then(response => {
                // console.log('response:',response)
                if (response.data.status == 1 && response.data.message == 'success') {
                    return resolve(response.data)
                }
                else {
                    return reject(response.data)
                }
            })
            .catch(error => {
                // console.log('error:',error.response)
                return reject(error.response)
            })
        })
    },

    update(_form_data) {
        let form_data = new FormData()
        form_data.append('method', 'update_profile')
        form_data.append('user_id', _form_data.user_id)
        form_data.append('first_name', _form_data.first_name)
        form_data.append('last_name', _form_data.last_name)
        form_data.append('city', _form_data.city)
        form_data.append('state', _form_data.state)
        form_data.append('zip', _form_data.zip)

        return new Promise((resolve, reject) => {
            axios.post(
                '/api/v1/service', form_data
            )
            .then(response => {
                // console.log('response:',response)
                if (response.data.status == 1 && response.data.message == 'success') {
                    return resolve(response.data)
                }
                else {
                    return reject(response.data)
                }
            })
            .catch(error => {
                // console.log('error:',error.response)
                return reject(error.response)
            })
        })
    }
}