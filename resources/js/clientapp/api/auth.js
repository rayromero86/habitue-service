export default {
    tryLoggingIn(data) {
        let form_data = new FormData()
        form_data.append('username', data.username)
        form_data.append('password', data.password)

        return new Promise((resolve, reject) => {
            axios.post(
                '/api/v1/login', form_data
            )
            .then(response => {
                // console.log('response:',response)
                if (response.data.status == 1 && response.data.message == 'success') {
                    return resolve(response.data)
                }
                else {
                    return reject(response.data)
                }
            })
            .catch(error => {
                // console.log('error:',error.response)
                return reject(error.response)
            })
        })
    },

    signup(_form_data) {
        let form_data = new FormData()
        form_data.append('email', _form_data.email)
        form_data.append('password', _form_data.password)
        form_data.append('password_confirmation', _form_data.password_confirmation)
        form_data.append('first_name', _form_data.first_name)
        form_data.append('last_name', _form_data.last_name)
        form_data.append('city', _form_data.city)
        form_data.append('state', _form_data.state)
        form_data.append('zip', _form_data.zip)

        return new Promise((resolve, reject) => {
            axios.post(
                '/api/v1/signup', form_data
            )
            .then(response => {
                // console.log('response:',response)
                if (response.data.status == 1 && response.data.message == 'success') {
                    return resolve(response.data)
                }
                else {
                    return reject(response.data)
                }
            })
            .catch(error => {
                // console.log('error:',error.response)
                return reject(error.response)
            })
        })
    },
}