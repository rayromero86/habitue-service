export default {
    getAll() {
        let form_data = new FormData()
        form_data.append('method', 'get_products')

        return new Promise((resolve, reject) => {
            axios.post(
                '/api/v1/service', form_data
            )
            .then(response => {
                // console.log('response:',response)
                if (response.data.status == 1 && response.data.message == 'success') {
                    return resolve(response.data)
                }
                else {
                    return reject(response.data)
                }
            })
            .catch(error => {
                // console.log('error:',error.response)
                return reject(error.response)
            })
        })
    }
}