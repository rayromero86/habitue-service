import Vue from 'vue'


export function initialize(store, router) {
    router.beforeEach((to, from, next) => {
        const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
        const isAuth = store.getters['auth/isAuth'];
    
        if(requiresAuth && !isAuth) {
            next('/app');
        } else if(to.path == '/app/login' && isAuth) {
            next('/app/dashboard/dash');
        } else {
            next();
        }
    });
    
    axios.interceptors.response.use(null, (error) => {
        if (error.response.status == 401) {
            store.commit('auth/logout');
            router.push('/app');
        }

        return Promise.reject(error);
    });

    if (store.getters['auth/isAuth']) {
        setAuthorization(store.getters['auth/login_key']);
    }

    // add filter
    Vue.filter('truncate', function(text, length, clamp){
        clamp = clamp || '...';
        var node = document.createElement('div');
        node.innerHTML = text;
        var content = node.textContent;
        return content.length > length ? content.slice(0, length) + clamp : content;
    });
}

export function setAuthorization(token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
}