<?php

// use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/v1/login', 'Api\AuthController@login');
Route::post('/v1/signup', 'Api\RegisterController@register');

Route::post('/v1/service', 'Api\ServiceController@index');



Route::get('/test/test1', 'Api\TestController@test1');