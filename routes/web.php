<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



/**
 * Client Routes
 */
Route::get('/app', 'ClientAppController@index')->name('clientapp');
Route::get('/app/{any}', 'ClientAppController@index')->name('clientapp.any')->where('any','.*');
// Route::get('/app/{any1}/{any2}', 'ClientAppController@index')->name('clientapp.any.any');



/**
 * Admin Routes
 */
Route::get('/admin/products', 'Admin\ProductGoodController@products')->name('admin.products');
Route::get('/admin/product/add', 'Admin\ProductGoodController@add')->name('admin.product.add');
Route::post('/admin/product/store', 'Admin\ProductGoodController@storeInitial')->name('admin.product.store');
Route::get('/admin/product/show/{slug}', 'Admin\ProductGoodController@show')->name('admin.product.show');
Route::post('/admin/product/update', 'Admin\ProductGoodController@update')->name('admin.product.update');
Route::post('/admin/product/update_selection', 'Admin\ProductGoodController@update_selection')->name('admin.product.update.selection');


