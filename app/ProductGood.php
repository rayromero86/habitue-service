<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGood extends Model
{
    protected $table = "product_goods";
    protected $dates = [
        'deleted_at', 
        'created_at', 
        'updated_at'
    ];
    protected $fillable = [
        'slug',

        'name',
        'caption',
        'description',
        'image',

        'currency',
        'price',

        'selections',

        'activate_form_url',

        'hide',
        'disabled',
    ];
    protected $casts = [
        'selections' => 'array'
    ];


    
    public function seller()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    // public function productable()
    // {
    //     return $this->morphTo();
    // }



    public function scopeSlug($query, $slug) 
    {
        return $query->where('slug', '=', $slug);
    }



    // public static function generateSlug() 
    // {
    //     do {
    //         $slug = 'pr_'.str_random(40);
    //     } while(Product::Slug($slug)->exists());

    //     return $slug;
    // }
}
