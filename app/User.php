<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Laravel\Cashier\Billable;

use App\Traits\UserAttribute;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, Billable, UserAttribute;

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'trial_ends_at'];
    protected $hidden = [
        'id', 
        'role_id', 
        'password', 
        'remember_token', 
        'created_at', 
        'updated_at', 
        'activation_token', 
        'hide', 
        'disabled', 
        'deleted_at', 
        'stripe_id', 
        'card_brand', 
        'card_last_four', 
        'trial_ends_at',
    ];
    protected $fillable = [
        'slug',
        // 'role_id', 

        'name', 
        'first_name', 
        'last_name',
        'avatar', 

        'email', 
        'password', 

        'city', 
        'state', 
        'zip', 
 
        'activation_token', 

        'hide', 
        'disabled'
    ];
    protected $casts = ['created_at' => 'date:d-m-Y'];



    /**
     * Get all product-goods from this model
     */
    public function product_goods() 
    {
        return $this->hasMany('App\ProductGood', 'user_id');
        // return $this->morphMany('App\ProductGood', 'productable');
    }

    /**
     * Get all product-services from this model
     */
    public function product_services() 
    {
        return $this->morphMany('App\ProductService', 'productserviceable');
    }



    public function scopeSlug($query, $slug) 
    {
        return $query->where('slug', '=', $slug);
    }
}
