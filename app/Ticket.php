<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = "tickets";
    protected $dates = [
        'deleted_at', 
        'created_at', 
        'updated_at'
    ];
    protected $fillable = [
        'slug',
        'user_id',
        'ref_id',

        'title',
        'message',

        'hide',
        'disabled',
    ];



    public function user() 
    {
        return $this->belongsTo('App\User', 'user_id');
    }



    public function scopeSlug($query, $slug) 
    {
        return $query->where('slug', '=', $slug);
    }
}
