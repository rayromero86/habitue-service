<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use App\Http\Resources\ProductGoodCollection;
use App\Http\Resources\TicketResource;
use App\Http\Resources\TicketCollection;

use App\User;
use App\ProductGood;
use App\Ticket;

// use App\Http\Requests\UpdateUserProfileRequest;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api']);
    }

    public function index(Request $request)
    {
        $method = $request->input('method');

        if (!$method) {
            return $this->empty_request();
        }

        switch ($method) {
            case 'get_users':
                return $this->get_users($request);
                break;

            case 'get_user_by_id':
                return $this->get_user_by_id($request);
                break;

            case 'update_profile':
                return $this->update_profile($request);
                break;

            case 'get_products':
                return $this->get_products($request);
                break;

            case 'get_tickets':
                return $this->get_tickets($request);
                break;

            case 'create_ticket':
                return $this->create_ticket($request);
                break;

            case 'get_billing_cards':
                return $this->get_billing_cards($request);
                break;

            case 'add_billing_card':
                return $this->add_billing_card($request);
                break;

            case 'checkout_product':
                return $this->checkout_product($request);
                break;

            default:
                return $this->unknown_method();
        }        
    }

    private function get_users(Request $request) 
    {
        $users = User::all();

        return new UserCollection($users);
    }

    private function get_user_by_id(Request $request) 
    {
        if (! $request->input('user_id')) {
            return $this->invalid_request();
        }

        $user = User::find($request->input('user_id'));

        if (! $user) {
            return $this->no_data_found();
        }

        return new UserResource($user);
    }

    private function update_profile(Request $request)
    {
        $request->validate([
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50'
        ]);

        $user = User::find($request->input('user_id'));

        $user->update($request->all());

        $user = User::find($request->input('user_id'));
        
        return response()->json([
            'status' => 1,
            'message' => 'success',
            'request' => $request->all(),
            'user' => $user
        ], 200);
    }

    private function get_products(Request $request)
    {
        $products = ProductGood::all();

        return new ProductGoodCollection($products);
    }

    private function get_subscriptions(Request $request)
    {
        // for getting subscriptions
    }

    private function get_tickets(Request $request)
    {
        $tickets = Ticket::orderBy('id', 'desc')->get();

        return new TicketCollection($tickets);
    }

    private function register_stripe_customer(User $user)
    {
        if ($user && !$user->stripe_id) {
            // \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
            $stripe = \Stripe::make(config('services.stripe.secret'));

            // Stripe\Customer
            $customer = $stripe->customers()->create([
                'email' => $user->email,
            ]);

            return true;
        }
        else {
            return false;
        }
    }

    private function create_ticket(Request $request)
    {
        do {
            $slug = 'tk_'.str_random(30);
        } while(Ticket::Slug($slug)->exists());

        $ticket = Ticket::create([
            'slug' => $slug,
            'ref_id' => $request->input('ref_id'),
            'title' => $request->input('title')
        ]);

        return response()->json([
            'status' => 1,
            'message' => 'success',
            'data' => $ticket
        ], 200);
        
    }

    private function get_billing_cards(Request $request)
    {
        $user = User::findOrFail($request->input('user_id'));

        // check if user is already registered to stripe customer
        if (!$user->stripe_id) {
            if ($this->register_stripe_customer($user)) {
                $user->refresh();
            }
            else {
                // error response
                return response()->json([
                    'status' => 0,
                    'message' => 'Cannot find stripe customer account!'
                ], 401);
            }
        }

        // Get all stripe billing cards
        $stripe = \Stripe::make(config('services.stripe.secret'));

        $cards = $stripe->cards()->all($user->stripe_id);

        return response()->json([
            'status' => 1,
            'message' => 'success',
            'data' => [
                'cards' => $cards
            ],
        ], 200);
    }

    private function add_billing_card(Request $request)
    {
        $user = User::findOrFail($request->input('user_id'));

        // check if user is already registered to stripe customer
        if (!$user->stripe_id) {
            if ($this->register_stripe_customer($user)) {
                $user->refresh();
            }
            else {
                // error response
                return response()->json([
                    'status' => 0,
                    'message' => 'Cannot find stripe customer account!'
                ], 401);
            }
        }

        $stripe = \Stripe::make(config('services.stripe.secret'));

        $token = $stripe->tokens()->create([
            'card' => [
                'number'    => $request->input('number'),
                'exp_month' => $request->input('exp_month'),
                'cvc'       => $request->input('cvc'),
                'exp_year'  => $request->input('exp_year'),
            ],
        ]);
        
        $card = $stripe->cards()->create($user->stripe_id, $token['id']);

        return response()->json(array_merge($request->all(), [
            'status' => 1,
            'message' => 'success',
            'data' => [
                'card' => $card
            ]
        ]), 200);
    }

    private function checkout_product(Request $request)
    {
        return response()->json(array_merge($request->all(), [
            'status' => 1,
            'message' => 'success'
        ]), 200);
    }

























    private function unknown_method() 
    {
        return response()->json([
            'status' => -1,
            'message' => 'unknown method of request',
        ], 401);
    }

    private function empty_request()
    {
        return response()->json([
            'status' => -1,
            'message' => 'empty method of request',
        ], 401);
    }

    private function invalid_request()
    {
        return response()->json([
            'status' => -1,
            'message' => 'invalid request',
        ], 401);
    }

    private function no_data_found()
    {
        return response()->json([
            'status' => -1,
            'message' => 'no data found',
        ], 401);
    }

    public function test()
    {
        return response()->json([
            "data1" => 'value1',
            "data2" => 'value2',
            "data3" => 'value3',
        ], 200);
    }
}
