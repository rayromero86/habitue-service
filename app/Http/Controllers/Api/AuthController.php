<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Validator;

use App\User;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('logout');
    }

    /**
     * Login user
     * @param Request $request
     * @param [string] username
     * @param [string] password
     * @param [boolean] remember_me
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        if ($validator->fails()) { 
            return response()->json([
                'error' => $validator->errors()
            ], 422);
        }

        $credentials = [
            'email' => $request->input('username'), 
            'password' => $request->input('password')
        ];

        if(!Auth::attempt($credentials)) {
            return response()->json([
                'status' => -1,
                'message' => 'Unauthorized',
                'error' => [
                    'custom' => ['Invalid username or password']
                ],
            ], 401);
        }

        $user = $request->user();

        $tokenResult = $user->createToken($user->name.' api');

        // $token = $tokenResult->token;
        // if ($request->remember_me) {
        //     $token->expires_at = Carbon::now()->addDays(5);
        // }
        // $token->save();

        return response()->json([
            'method' => 'login',
            'username' => $user->email,
            'status' => 1,
            'message' => 'success',
            'login_key' => $tokenResult->accessToken,
            'data' => [
                'type' => 'user',
                'id' => $user->id,
                'slug' => $user->slug,
                'attributes' => [
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'email' => $user->email,
                ],
            ],
        ], 201);
    }
}
