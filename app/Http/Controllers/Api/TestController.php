<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function test1()
    {
        return response()->json([
            'data1' => 'value for data1',
            'data2' => 'value for data2',
            'data3' => 'value for data3',
        ], 200);
    }
}
