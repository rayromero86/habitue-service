<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClientAppController extends Controller
{
    public function index() 
    {
        return view('client.index');
    }
}
