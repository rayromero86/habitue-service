<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use Cartalyst\Stripe\Stripe;

use App\ProductGood;

use App\Http\Requests\StoreProductGoodRequest;
use App\Http\Requests\UpdateProductGoodRequest;

class ProductGoodController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function products()
    {
        $user = Auth::user();

        $products = $user->product_goods;

        // dd($products);
        return view('admin.products.products', compact('products'));
    }

    public function show($slug)
    {
        $product = ProductGood::Slug($slug)->first();

        return view('admin.products.show', compact('product'));
    }

    public function add()
    {
        return view('admin.products.add');
    }

    public function storeInitial(StoreProductGoodRequest $request)
    {
        $product = Auth::user()->product_goods()->create([
            'slug' => $this->generateSlug(),
            'name' => $request->input('name'),
            'caption' => $request->input('caption'),
            'description' => $request->input('description'),
            'selections' => '[]'
        ]);

        $this->storeImageFileToDisk($request, $product);

        $stripe_product = $this->storeProductToStripe($product);

        // update stripe-product-id after stripe creation
        $product->stripe_product_id = $stripe_product['id'];
        $product->save();

        if ($product) {
            return response()->json([
                'success' => true,
                'product' => $product,
            ], 201);
        }
    }

    public function storeImageFileToDisk(StoreProductGoodRequest $request, ProductGood $product)
    {
        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $path = 'storage/products/';
            if(! \File::isDirectory($path)) {
                \Storage::makeDirectory('public/products');
            }

            // prepare image filename
            $extension = $request->image->getClientOriginalExtension();
            $str_random = str_random(20);
            $filename = $str_random.'.'.$extension;

            // store image to storage
            $request->image->storeAs('products', $filename, 'public');

            // update product database
            $product->image = $path.$filename;
            $product->save();

            return;
        }
        else {
            return false;
        }
    }

    private function storeProductToStripe(ProductGood $product)
    {
        // handles to store the this record to stripe server
        $stripe = new Stripe(config('services.stripe.secret'));
        
        $product = $stripe->products()->create([
            'name'          => $product->name,
            'caption'       => $product->caption,
            'description'   => $product->description,
            'images'        => [url('/') . $product->image],
            'active'        => true,
            'shippable'     => false,
        ]);

        return $product;
    }

    public function update(UpdateProductGoodRequest $request)
    {
        $product = ProductGood::find($request->input('id'));

        $product->update($request->all());

        $product->refresh();

        $this->updateProductToStripe($product);

        // $product = ProductGood::find($request->input('id'));
        
        return response()->json([
            'success' => true,
            'product' => $product
        ], 201);
    }

    private function updateProductToStripe(ProductGood $product)
    {
        // handles to update the product to stripe
        $stripe = new Stripe(config('services.stripe.secret'));

        $product = $stripe->products()->update($product->stripe_product_id, [
            'name'          => $product->name,
            'caption'       => $product->caption,
            'description'   => $product->description,
            'images'        => [$product->image],
            'active'        => true,
            'shippable'     => false,
        ]);

        return;
    }

    public function update_selection(Request $request)
    {
        $updated = ProductGood::find($request->input('id'))->update([
            'selections' => $request->input('selections'),
        ]);

        if ($updated) {
            return response()->json([
                'success' => true,
                'product' => ProductGood::find($request->input('id')),
            ], 201);
        }
        else {
            return response()->json([
                'success' => false,
            ], 401);
        }
    }

    private function generateSlug() 
    {
        do {
            $slug = 'pg_'.str_random(20);
        } while(ProductGood::Slug($slug)->exists());

        return $slug;
    }
    
}
