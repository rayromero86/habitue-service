<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'type' => 'user',
            'id' => (string) $this->id,
            'attributes' => [
                'slug' => $this->slug,
                'role_id' => $this->role_id,
                'name' => $this->name,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'email' => $this->email,
                'avatar' => $this->avatar,
                'city' => $this->city,
                'state' => $this->state,
                'zip' => $this->zip,
                'created_at' => (string) $this->created_at,
                'update_at' => (string) $this->updated_at,
            ],
        ];
    }

    public function with($request)
    {
        return [
            'status' => 1,
            'message' => 'success'
        ];
    }
}
