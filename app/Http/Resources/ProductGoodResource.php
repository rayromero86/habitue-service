<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductGoodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
        return [
            'type' => 'product',
            'id' => (string) $this->id,
            'attributes' => [
                'user_id' => $this->user_id,
                'stripe_product_id' => $this->stripe_product_id,
                'name' => $this->name,
                'caption' => $this->caption,
                'description' => $this->description,
                'image' => $this->image,
                'currency' => $this->currency,
                'price' => $this->price,
                'selections' => $this->selections,
                'hide' => $this->hide,
                'disabled' => $this->disabled,
                'created_at' => (string) $this->created_at,
                'update_at' => (string) $this->updated_at,
            ],
        ];
    }

    public function with($request)
    {
        return [
            'status' => 1,
            'message' => 'success'
        ];
    }
}
