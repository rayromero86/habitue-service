<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductGoodCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'data' => ProductGoodResource::collection($this->collection),
        ];
    }

    public function with($request)
    {
        return [
            'data_count' => ProductGoodResource::collection($this->collection)->count(),
            'status' => 1,
            'message' => 'success',
        ];
    }
}
