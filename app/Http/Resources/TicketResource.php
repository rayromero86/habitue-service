<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'type' => 'ticket',
            'id' => (string) $this->id,
            'attributes' => [
                'slug' => $this->slug,
                'user_id' => $this->user_id,
                'ref_id' => $this->ref_id,
                'title' => $this->title,
                'message' => $this->message,
                'created_at' => (string) $this->created_at,
                'update_at' => (string) $this->updated_at,
            ],
        ];
    }

    public function with($request)
    {
        return [
            'status' => 1,
            'message' => 'success'
        ];
    }
}
