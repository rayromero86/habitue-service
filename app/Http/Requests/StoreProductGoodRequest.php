<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductGoodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:200|unique:product_goods',
            'caption' => 'max:50',
            'description' => 'max:1000',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Product name is required.',
            'name.max' => 'The product name must not be more than 200 characters',
            'name.unique' => 'You already have a product with that name',
            'caption.max' => 'The caption must not be more than 50 characters',
            'description.max' => 'The description must not be more than 1000 characters',
        ];
    }
}
