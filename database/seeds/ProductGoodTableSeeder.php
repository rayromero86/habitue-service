<?php

use Illuminate\Database\Seeder;

class ProductGoodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_goods')->insert([
            [
                'slug' => 'pg_'.str_random(20),
                'user_id' => 1,
                'name' => 'Product 01',
                'caption' => 'I am a caption',
                'description' => 'I am a description',
                'selections' => '"[{\"title\":\"How many funnel steps do you need?\",\"options\":[{\"option\":\"1 new funnel\",\"cost\":\"25\"},{\"option\":\"Choice 2\",\"cost\":\"110\"}]},{\"title\":\"How urgent do you need them?\",\"options\":[{\"option\":\"24 hours\",\"cost\":\"20\"},{\"option\":\"Standard\",\"cost\":\"8\"}]}]"',
            ],
        ]);
    }
}
