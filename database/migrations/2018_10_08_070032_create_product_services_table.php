<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 43)->unique();
            $table->string('stripe_product_id')->nullable();

            $table->string('productserviceable_type', 50)->nullable();
            $table->integer('productserviceable_id')->unsigned();

            $table->string('name', 100);
            $table->string('description', 250)->nullable();
            $table->string('image')->default('storage/subscriptions/default.png');

            $table->boolean('hide')->default(false);
            $table->boolean('disabled')->default(true);
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_services');
    }
}
