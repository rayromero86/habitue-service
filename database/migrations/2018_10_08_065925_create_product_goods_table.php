<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_goods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 23)->unique();
            $table->integer('user_id');
            $table->string('stripe_product_id')->nullable();

            $table->string('name', 100);
            $table->string('caption', 250)->nullable();
            $table->string('description', 1000)->nullable();
            $table->string('image')->default('storage/products/default.png');

            $table->string('currency')->default('usd');
            $table->unsignedDecimal('price')->default(1.00);

            // $table->json('selections')->default('[]');
            $table->text('selections')->nullable();

            $table->string('activate_form_url')->nullable();

            $table->tinyInteger('hide')->default(0);
            $table->tinyInteger('disabled')->default(1);
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_goods');
    }
}



//name: Product 01
//caption: i am a caption
//description: i am a desc
//selections: "[{\"title\":\"How many funnel steps do you need?\",\"options\":[{\"option\":\"1 new funnel\",\"cost\":\"25\"},{\"option\":\"Choice 2\",\"cost\":\"110\"}]},{\"title\":\"How urgent do you need them?\",\"options\":[{\"option\":\"24 hours\",\"cost\":\"20\"},{\"option\":\"Standard\",\"cost\":\"8\"}]}]"